# git workflow in Pythonista for iOS


    +---------------------------------------------+
    |StaSh inside Pythonista for iOS (iphone,ipad)|
    +---------------------------------------------+
                                |
                                | git push/pull
                                |
                        +-------------------------+
                        |server (desktop computer)|
                        +-------------------------+

## Setup ssh keys (passwordless login)

In StaSh:

    $ email=your.name@example.com #!!! <- put your e-mail here
    $ mkdir $STASH_ROOT/.ssh
    $ cd $STASH_ROOT/.ssh
    $ ssh-keygen
    $ mail -s 'public ssh key' -f id_rsa.pub $email

On the server:

    you@desktop$ cat id_rsa.pub | tee -a ~/.ssh/authorized_keys

Test in StaSh:

    $ gitserver=you@desktop #!!! <- put your server here
	$ ssh $gitserver date

## Create new repo from scratch on the server

In StaSh:

    $ project=git-workflow  #!!! <- put your project name here
    $ ssh $gitserver "mkdir ~/.bare-repos/$project.git &&
    				  cd    ~/.bare-repos/$project.git &&
    				  git init --bare"

## Clone the repo in StaSh

    $ user=you #!!! <- put your username here
    $ git clone git+ssh://$gitserver//home/$user/.bare-repos/$project.git $project

Note: `//` after the hostname (dulwich bug?)

Or to clone from bitbucket.org (assuming ssh key is added) in StaSh:

    $ git clone git@bitbucket.org:$user/$project.git $project

## Add some files, commit

Now add files to `$project` directory, edit the files then commit/push to the server:

	$ cd $project
    $ git add README.md
    $ git commit 'Initial commit'

## Push back to the server

    $ git push

## Create a new project in one step automatically

Or run `create-new-project $project` in StaSh to automate all the
steps: create bare repo on the server, clone the repo in StaSh, add
dummy `README.md`, commit, push back to the server.

## Pull the code from the server

    $ git pull

## Add existing git repo to github

Create a new repo on github (assuming ssh key is added):

    $ gh create -i -s "$project description" $project

* `-i`: enable Github Issues
* `-s`: add description

Add remote:

    $ git remote origin git@github.com:$user/$project.git

Note: no 'add' after 'remote' in the command (unlike the standard git cli).

Push to the given remote:

    $ git push origin

'origin' name may be omitted:

    $ git push
