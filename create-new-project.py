#!python2
"""Usage: create-new-project <project-slug> [-i]

Run inside StaSh.

1. Create a bare git repo on the server (execute command via ssh on the desktop)
2. Clone it
"""
import sys

try:
    sys.argv.remove('-i')
    add_readme = True
except ValueError:
    add_readme = False

projects_root = '~/Documents/prj'
project = sys.argv[1]
user = 'me'
gitserver = '{user}@me'.format(**vars())

_stash('''ssh {gitserver} 'mkdir ~/.bare-repos/{project}.git && cd    ~/.bare-repos/{project}.git && git init --bare'
cd {projects_root} 
git clone git+ssh://{gitserver}//home/{user}/.bare-repos/{project}.git {project}
cd {project}
'''.format(**vars()))

if add_readme:
    _stash('''cd {projects_root}/{project}
echo "#{project}" > README.md
git add README.md
git commit "Initial commit"
git push
'''.format(**vars()))
